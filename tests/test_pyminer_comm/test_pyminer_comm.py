from pyminer.pyminer_comm import set_vars, set_var, get_vars, get_var_names, get_var, get_style_sheet, get_settings, \
    modify_settings
from pyminer.pyminer_comm.base import get_protocol, is_pyminer_service_started, DataDesc


def test_data_transfer():
    if is_pyminer_service_started():
        print(get_protocol())
        test_list = [1, 2, 3, 4, 5, 6, 7, 8]
        set_vars({'a': 123, 'b': 3456, 'c': test_list})
        variables = get_vars(['a', 'b', 'c'])
        assert variables['a'] == 123
        assert variables['b'] == 3456

        assert variables['c'] == test_list
        set_var('a', 4567)
        assert get_var('a') == 4567
        assert get_var('c') == test_list
        vars_in_workspace = get_var_names()
        for var_name in ['a', 'b', 'c']:
            assert var_name in vars_in_workspace
    else:
        raise ProcessLookupError("PyMiner未启动")


def test_data_desc():
    if is_pyminer_service_started():
        set_var('desc_a', DataDesc(123))
        set_var('desc_b', DataDesc([1, 2, 3, 4]))
    else:
        raise ProcessLookupError("PyMiner未启动")


def test_configs():
    if is_pyminer_service_started():
        print(get_settings())
        assert isinstance(get_style_sheet(), str)
        # modify_settings({'theme': 'Fusion'})
        # modify_settings({'theme': 'QDarkStyle'})
    else:
        raise ProcessLookupError('PyMiner未启动')
