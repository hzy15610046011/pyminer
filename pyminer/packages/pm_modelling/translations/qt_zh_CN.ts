<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>PMModellingToolBar</name>
    <message>
        <location filename="../main.py" line="21"/>
        <source>Model Selection</source>
        <translation>模型选择</translation>
    </message>
    <message>
        <location filename="../main.py" line="24"/>
        <source>Decision tree</source>
        <translation>决策树</translation>
    </message>
    <message>
        <location filename="../main.py" line="26"/>
        <source>ScoreCard</source>
        <translation>评分卡</translation>
    </message>
    <message>
        <location filename="../main.py" line="29"/>
        <source>Classify</source>
        <translation>分类</translation>
    </message>
    <message>
        <location filename="../main.py" line="31"/>
        <source>Regression</source>
        <translation>回归</translation>
    </message>
    <message>
        <location filename="../main.py" line="34"/>
        <source>Clustering</source>
        <translation>聚类</translation>
    </message>
    <message>
        <location filename="../main.py" line="36"/>
        <source>Dimension Reduction</source>
        <translation>降维</translation>
    </message>
    <message>
        <location filename="../main.py" line="69"/>
        <source>Modelling</source>
        <translation>模型</translation>
    </message>
</context>
</TS>
